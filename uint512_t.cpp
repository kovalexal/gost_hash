#include "uint512_t.h"

uint512_t::uint512_t(const uint32_t &value)
{
    contents[63] = (uint8_t) (value & 0x000000ff);
    contents[62] = (uint8_t) ((value & 0x0000ff00) >> 8);
    contents[61] = (uint8_t) ((value & 0x00ff0000) >> 16);
    contents[60] = (uint8_t) ((value & 0xff000000) >> 24);
    memset(contents, (uint8_t) 0, 60);
}

uint512_t::uint512_t(const char *raw_contents)
{
    // Reverse byte order (input is in big endian)
    // for (int i = 0; i < 64; ++i)
    //    memcpy(contents + 64 - i - 1, raw_contents + i, 1);

    // Don`t reverse byte order (input is in little endian)
    memcpy(contents, raw_contents, 64);
}

uint512_t::uint512_t(const uint512_t &other)
{
    memcpy(contents, other.contents, 64);
}

uint512_t& uint512_t::operator=(const uint512_t &other)
{
    memcpy(contents, other.contents, 64);
    return *this;
}

const uint512_t uint512_t::operator+(const uint512_t &other) const
{
    uint512_t result;
    uint16_t current_byte, carry_over = 0;

    for (int i = 63; i >= 0; i--) {
        current_byte = (uint16_t) contents[i] + (uint16_t) other.contents[i] + carry_over;
        carry_over = current_byte >> 8;
        result.contents[i] = (uint8_t) (current_byte & ((uint16_t) 0xFF));
    }
    return result;
}

uint512_t& operator+=(uint512_t& left, const uint512_t& right)
{
    left = left + right;
    return left;
}

const uint512_t uint512_t::operator^(const uint512_t &other) const
{
    uint512_t result;

    for (int i = 0; i < 64; ++i)
        result.contents[i] = this->contents[i] ^ other.contents[i];

    return result;
}

uint512_t& operator^=(uint512_t& left, const uint512_t& right)
{
    left = left ^ right;
    return left;
}

std::ostream& operator<<(std::ostream& os, const uint512_t& number)
{
    char hexdigest[129];
    const unsigned char *pin = number.contents;
    const char *hex = "0123456789abcdef";
    char *pout = hexdigest;
    for (int i = 0; i < 64; i++) {
        const unsigned char *pin = number.contents + i;
        hexdigest[2 * i] = hex[(*pin >> 4) & 0xF];
        hexdigest[2 * i + 1] = hex[(*pin) & 0xF];
    }
    hexdigest[128] = '\0';
    os << hexdigest;
    return os;
}
