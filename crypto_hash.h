#ifndef CRYPTO_HASH_H
#define CRYPTO_HASH_H
#include <cstdint>
#include "uint512_t.h"

//char hexdigest[129]
void get_hash(const char *buffer, int size, char *hexdigest);

#endif //CRYPTO_HASH_H