#ifndef GOST_HASH_UINT512_T_H
#define GOST_HASH_UINT512_T_H

#include <cstdint>
#include <cstring>
#include <iostream>

/**
* 512 byte int in little endian byte order (mainly for Intel architecture)
*/
class uint512_t
{
public:
    // Raw bytes in little endian order
    uint8_t contents[64];

    // Basic constructor from 32-bit integer number
    uint512_t(const uint32_t &value = 0);
    
    // Constructor from raw bytes
    uint512_t(const char *raw_contents);

    // Copy constructor
    uint512_t(const uint512_t &other);

    // Operator =
    uint512_t& operator=(const uint512_t &other);

    // Operator +
    const uint512_t operator+(const uint512_t &other) const;
    friend uint512_t& operator+=(uint512_t& left, const uint512_t& right);

    // Operator ^ (xor)
    const uint512_t operator^(const uint512_t &other) const;
    friend uint512_t& operator^=(uint512_t& left, const uint512_t& right);

    // Operator << (output)
    friend std::ostream& operator<<(std::ostream& os, const uint512_t& dt);

};


#endif //GOST_HASH_UINT512_T_H
